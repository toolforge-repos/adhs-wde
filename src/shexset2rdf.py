import os, json, argparse, sys, datetime, re
from pyshexc.parser_impl.generate_shexj import parse
from antlr4 import InputStream
from jsonasobj import as_json
from rdflib import Graph, RDF, RDFS
from rdflib.term import URIRef, Literal
import redis, MySQLdb
import urllib.request as ureq
"""
Find 'shexset' in the given directory aliases and convert the listed files
to RDF. Present the RDF as graph on Redis
"""
def process(E, schema):
    E = 'E' + str(E)
    print('-----processing {}'.format(E), file=sys.stderr)
    g = Graph()
    newschema = ''
    # make sure BASE is set to http://www.wikidata.org/wiki/EntitySchema:Exyz in each Exyz file
    base_is_set = False
    for line in schema.split('\n'):
        if line.startswith('BASE '):
            line = 'BASE <' + WIKIBASE + E + '>'
            base_is_set = True
        newschema = newschema + line + '\n'
    schema = newschema if base_is_set else 'BASE <' + WIKIBASE + E + '>\n' + newschema
    try:
        shexj = parse(InputStream(schema))
    except ValueError:
        shexj = None
    if shexj is None:
        return (False, '')
    shexj['@context'] = "http://www.w3.org/ns/shex.jsonld"
    shexj['@id'] = WIKIBASE + E
    g.parse(data=as_json(shexj, indent=None), format="json-ld")
    print('graph size: {}'.format(len(g)))
    # put original ShExC code as rdfs:comment into schema
    g.add( (URIRef(WIKIBASE+E), RDFS.comment, Literal(schema)) )
    rdf = g.serialize(format='json-ld')
    print('RDF size: {}'.format(len(rdf)))
    return (True, rdf)

def chunked_Es(the_set, size):
    try:
        while True:
            s = set()
            for i in range(size):
                s.add(the_set.pop())
            yield s
    except KeyError:
        if len(s) > 0:
            yield s
        return

def remove_from_db(db, E):
    c=db.cursor()
    c.execute("DELETE FROM {} WHERE number = {}".format(DBTABLE, E))
    c.close()

def add_to_db(db, E, pid, revid, is_valid, rdf):
    c=db.cursor()
    cmd = 'INSERT INTO {} (number, page_id, lastrevid, is_valid_rdf, rdf) VALUES (%s,%s,%s,%s,%s)'.format(DBTABLE)
    c.execute(cmd, (E, pid, revid, is_valid, rdf))
    if args.debug:
        print(cmd)
    c.close()

def update_db(db, E, is_valid, rdf):
    c=db.cursor()
    cmd = 'UPDATE {} SET is_valid_rdf = %s, rdf = %s WHERE number = {}'.format(DBTABLE, E)
    c.execute(cmd, (is_valid, rdf))
    c.close()

# Initiate the parser
parser = argparse.ArgumentParser(conflict_handler='resolve')
parser.add_argument("--help", '-?', action="help")
parser.add_argument("dir", help="directory to work on", nargs='?', default=None)
parser.add_argument("--debug", action="store", default=False, help="use local db")
parser.add_argument("--user", '-u', action="store")
parser.add_argument("--passwd", '-p', action="store")

# Read arguments from the command line
args = parser.parse_args()
if args.debug:
    DBHOST = "localhost"
    DBTABLE = "schema_0"
    DBUSER = "root"
    DBPASSWD = "vbnm"
    DBNAME = "test1"
    REDIS_HOST = "localhost"
else:
    if args.user is None or args.passwd is None:
        print('Missing credentials')
        parser.parse_args(['--help'])
        exit()
    DBHOST = 'tools.db.svc.eqiad.wmflabs'
    DBUSER = args.user
    DBPASSWD = args.passwd
    DBNAME = "s54607__eschema_p"
    DBTABLE = "schema_z"
    REDIS_HOST = "tools-redis.svc.eqiad.wmflabs"

REDIS_PREFIX = "QHGIgf1dySAJKTRfK2e2:shexset2rdf:"
Edict = dict()
actualEdict = dict()
pageids = dict()
WIKIBASE = 'http://www.wikidata.org/wiki/EntitySchema:'

if args.dir is None:
    # no path was given, so we fetch all page information, and fetch only changed pages
    # first open the database, it was created via 
    # create table DBTABLE (number int, page_id bigint, lastrevid bigint, is_valid_rdf boolean, rdf mediumtext, primary key(number));

    db = MySQLdb.connect(DBHOST,DBUSER,DBPASSWD,DBNAME)
    db.query("SELECT number, page_id, lastrevid FROM {}".format(DBTABLE))                         
    res = list(db.store_result().fetch_row(maxrows=0, how=1))
    for E in res:
        n = E.get('number')
        if n in Edict.keys():
            print('dupe: {}'.format(n))
            exit()
        Edict[n] = (E.get('page_id'), E.get('lastrevid'))
    print('read {} entities from {}'.format(len(Edict), DBHOST))

    # fetch
    api_allpages = 'action=query&list=allpages&apprefix=&apnamespace=640&aplimit=500&format=json'
    api_allpages_cont = 'action=query&list=allpages&apprefix=&apnamespace=640&aplimit=500&format=json&apcontinue='
    api_pageinfo = 'action=query&prop=info&format=json&pageids='
    url_schematext = 'https://www.wikidata.org/wiki/Special:EntitySchemaText/'
    baseuri = 'https://www.wikidata.org/w/api.php?'
    do_continue = True
    allp = api_allpages
   
    # get allpages (nr + pageid) in chunks of 500
    while do_continue:
        with ureq.urlopen(baseuri + allp) as f:
            qres = f.read().decode('utf-8')
        if qres is None:
            print(baseuri + allp)
            raise
        jol = json.loads(qres)
        do_continue = jol.get('continue') is not None
        page_list = jol.get('query').get('allpages')
        for d in page_list:
            title = d.get('title')
            E = int(title[title.rfind(':E')+2:])
            pid = d.get('pageid')
            if actualEdict.get(E) is not None:
                print('duplicate E{}: pageid {} and {}'.format(E, actualEdict.get(E), pid))
                exit()
            if pageids.get(pid) is not None:
                print('duplicate pageid: {}'.format(pid))
                exit()
            actualEdict[E] = pid
            pageids[pid] = E
        if do_continue:
            allp = api_allpages_cont + jol.get('continue').get('apcontinue')
    
    # handle differences of existence of Exyz
    diff1 = set(actualEdict.keys()).difference(Edict.keys())       # new EntitySchemas
    diff2 = set(Edict.keys()).difference(actualEdict.keys())       # removed since last poll
    intersec = set(actualEdict.keys()).intersection(Edict.keys())   # these were here before
    print('read {} entities from Wikidata: {} removed, {} added'.format(len(actualEdict), 
        len(diff2), len(diff1)))
    #print(Edict)
    #print(actualEdict)
    #print(intersec)
    #db.close()
    #exit()
    for E in diff2:
        remove_from_db(db, E)
    
    # check all Es in intersec for changed lastrevid
    # at the same time query the new pageids and revids
    for chunk in chunked_Es(intersec.union(diff1), 50):
        chunk_str = '|'.join(list(str(actualEdict.get(E)) for E in chunk))
        with ureq.urlopen(baseuri + api_pageinfo + chunk_str) as f:
            qres = f.read().decode('utf-8')
        print(baseuri + api_pageinfo + chunk_str)
        if qres is None:
            raise
        jol = json.loads(qres)
        pages = jol.get('query').get('pages')
        for ent in pages.values():
            pid = ent.get('pageid')
            title = ent.get('title')
            E = int(title[title.rfind(':E')+2:])
            lrvid = ent.get('lastrevid')
            print(pid,title,E,lrvid)
            if E in intersec:
                db_pid, db_lrvid = Edict.get(E)
                if db_pid != pid:
                    print('pid mismatch, E{}: {}, actually {}'.format(E, db_pid, pid))
                    exit()
                if db_lrvid != lrvid:
                    # fetch and set changed E
                    with ureq.urlopen(url_schematext + 'E' + str(E)) as f:
                        qres = f.read().decode('utf-8')
                        if qres is None:
                            raise
                        is_valid, rdf = process(E, qres)
                        update_db(db, E, is_valid, rdf)
            else:
                # fetch and set new E
                with ureq.urlopen(url_schematext + 'E' + str(E)) as f:
                    qres = f.read().decode('utf-8')
                    if qres is None:
                        raise
                    is_valid, rdf = process(E, qres)
                    add_to_db(db, E, pid, lrvid, is_valid, rdf)
    db.commit()
    db.close()
    exit()
else:
    # load RDF from directory (it all started here)
    dir = os.path.dirname(args.dir) + '/'
    Ereg = re.compile(r"E\d+$")

    # read shexset file in dir and fill Edict
    with open(dir + 'shexset', 'r') as setfile:
        lines = setfile.readlines()
        for l in lines:
            m = re.fullmatch(Ereg, l.rstrip())
            if m:
                E = m.group(0)
                with open(dir + E, 'r') as f:
                    Edict[E] = f.read()
            else:
                print("{} does not match: {}".format(l, m))


# move to Redis
#r = redis.StrictRedis(host=REDIS_HOST, port=6379, db=0)
#r.set(REDIS_PREFIX+'graph', data)

