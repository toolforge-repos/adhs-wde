# modified from https://github.com/nareike/adhs
#!/usr/bin/env python
from flask import Flask, request, render_template, redirect
from flask_cors import CORS
from flask_negotiate import consumes, produces
from adhs_response import *
from rdflib import Graph
import argparse, re, sys, os, redis, MySQLdb

# Initiate the parser
parser = argparse.ArgumentParser(conflict_handler='resolve')
parser.add_argument("--help", '-?', action="help")
parser.add_argument("--debug", action="store_true", default=False, help="use local db")
parser.add_argument("--dbuser", '-u', action="store")
parser.add_argument("--dbpasswd", '-P', action="store")
parser.add_argument('--file', type=str)
parser.add_argument('--host', default='127.0.0.1', type=str)
parser.add_argument('-p', '--port', default=5000, type=int)
parser.add_argument('-i', '--input', default='n3', choices=[
        'html',
        'hturtle',
        'mdata',
        'microdata',
        'n3',
        'nquads',
        'nt',
        'rdfa',
        'rdfa1.0',
        'rdfa1.1',
        'trix',
        'turtle',
        'xml'
    ], help='Optional input format')

# Read arguments from the command line
args = parser.parse_args()
if args.debug:
    DBHOST = "localhost"
    DBTABLE = "schema_0"
    DBUSER = "root"
    DBPASSWD = "vbnm"
    DBNAME = "test1"
    REDIS_HOST = "localhost"
else:
    if args.dbhost is None or args.dbuser is None or args.dbpasswd is None:
        print('Missing credentials')
        parser.parse_args(['--help'])
        exit()
    DBHOST = 'tools.db.svc.eqiad.wmflabs'
    DBUSER = args.dbuser
    DBPASSWD = args.dbpasswd
    DBNAME = "s54607__eschema_p"
    DBTABLE = "schema_z"
    REDIS_HOST = "tools-redis.svc.eqiad.wmflabs"

SCHEMABASE = 'https://www.wikidata.org/wiki/EntitySchema:'
REDIS_PREFIX = "QHGIgf1dySAJKTRfK2e2:shexset2rdf:"

# doesn't seem right to include */* ?
_FORMATS = ['*/*', 'text/html', 'application/sparql-results+json', 'application/sparql-results+xml']

Edict = {}
db = MySQLdb.connect(DBHOST,DBUSER,DBPASSWD,DBNAME, charset='utf8')
db.query("SELECT number, is_valid_rdf, rdf FROM {}".format(DBTABLE))
res = list(db.store_result().fetch_row(maxrows=0, how=1))
for E in res:
    n = E.get('number')
    if n in Edict.keys():
        print('dupe: {}'.format(n))
        exit()
    if E.get('is_valid_rdf'):
        Edict[n] = E.get('rdf')
    else:
        Edict[n] = None
print('read {} entities from {}'.format(len(Edict), DBHOST))
defect = ', '.join(list(str(E) for E in Edict.keys() if Edict.get(E) is None))
print('entries with invalid ShExC: {}'.format(defect))
#r = redis.StrictRedis(host=REDIS_HOST, port=6379, db=0)
#j = r.get(REDIS_PREFIX+'graph')

g = Graph()
for E in Edict.keys():
    rdf = Edict.get(E)
    if rdf is not None:
        g.parse(data=rdf, format='json-ld')
print('graph len: {}'.format(len(g)))
 
# set up a micro service using flash
app = Flask(__name__, static_url_path='')
app.debug = True
cors = CORS(app)

@app.route("/", methods=['GET', 'POST'])
def index():
    return redirect('/sparql')

@app.route("/sparql", methods=['GET'])
@produces(*_FORMATS)
def sparql_get():
    if 'query' in request.args:
        content = content_override(request.args)
        if content == None:
            content = get_pref_content_type(request)
        qres = g.query(request.args['query'])
        return get_response(qres, content)
    else:
        return render_template('sparql.html', src=args.file, port=request.host)

@app.route("/sparql", methods=['POST'])
@consumes('application/x-www-form-urlencoded')
@produces(*_FORMATS)
def sparql_post():
    if 'query' in request.form:
        content = content_override(request.form)
        if content == None:
            content = get_pref_content_type(request)
        qres = g.query(request.form['query'])
        return get_response(qres, content)
    else:
        return render_template('sparql.html', src=args.file, port=request.host)

def content_override(args):
    if 'format' in args:
        return args['format']
    elif 'output' in args:
        return args['output']
    else:
        return None

def get_pref_content_type(request):
    # exclude */* from FORMATS
    best = request.accept_mimetypes.best_match(_FORMATS[1:])
    # with no clear preference, always return text/html
    if request.accept_mimetypes[best] > request.accept_mimetypes['text/html']:
        return best
    else:
        return 'text/html'

if __name__ == "__main__":
    app.run(host=args.host,port=args.port)

